import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://surlybikes.com/uploads/bikes/Lowside_BK0534_Background-1200x800.jpg'),
    NetworkImage('https://cdn.boatinternational.com/files/2021/07/7551f070-eece-11eb-8a7a-21e9b190dca0-Tecnomar-for-Lamborghini-2.jpg'),
    NetworkImage('https://static.bangkokpost.com/media/content/20210707/c1_2144915.jpg'),
    NetworkImage('https://www.extremetech.com/wp-content/uploads/2019/12/SONATA-hero-option1-764A5360-edit-640x354.jpg'),
    NetworkImage('https://www.prachachat.net/wp-content/uploads/2021/11/S__25796639.jpg'),
    NetworkImage('https://www.running.ca/wp-content/uploads/2019/02/http___prod.static9.net_.au___media_Network_Images_2016_12_16_13_48_woman-sprinting-161216.jpg'),
    NetworkImage('https://cdn-icons-png.flaticon.com/512/224/224581.png'),
    NetworkImage('https://cdn.motor1.com/images/mgl/RPV89/s1/ford-transit.jpg'),
    NetworkImage('https://media.npr.org/assets/img/2014/05/27/walking-elderly-b007488a8980f05cbcd6a112741fe687ce3d3670-s1100-c50.jpg')
  ];


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('ลิสวิว'),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(
                    backgroundImage: images[index],
                  ),
                  title: Text(
                    '${titles[index]}',
                    style: TextStyle(fontSize: 18),
                  ),
                  subtitle: Text(
                    'There are many passengers in vehicles',
                    style: TextStyle(fontSize: 15),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    AlertDialog alert = AlertDialog(
                      title: Text('Welcome'),
                      // To display the title it is optional
                      content: Text('This is a ${titles[index]}'),
                      // Message which will be pop up on the screen
                      // Action widget which will provide the user to acknowledge the choice
                      actions: [
                        ElevatedButton(
                          // FlatButton widget is used to make a text to work like a button

                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          // function used to perform after pressing the button
                          child: Text('CANCEL'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text('ACCEPT'),
                        ),
                      ],
                    );
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      },
                    );
                  },
                ),
                Divider(
                  thickness: 0.8,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
